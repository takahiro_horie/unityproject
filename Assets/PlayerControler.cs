﻿using UnityEngine;
using System.Collections;

public class PlayerControler : MonoBehaviour {

	private float normalScale = 5f;
	private float selectedScale = 7f;
	private bool selectedFlg = false;

	private GUIStyle style;

	private GameObject target;
	private Vector3 targetPos;
	private int moveRange = 2;

	public GameObject marker;

	// Use this for initialization
	void Start () {
		style = new GUIStyle();
		style.normal.textColor = Color.white;
		style.fontSize = 30;
	}
	
	// Update is called once per frame
	void Update () {
		//選択されたとき
		if (this.gameObject == TouchManager.selectedGameObject) {
			selectedFlg = true;
		}
		//選択中、頭上にマーカー表示
		if(selectedFlg){
			transform.localScale = new Vector3(selectedScale, selectedScale, selectedScale);
			move();
		} else {transform.localScale = new Vector3(normalScale, normalScale, normalScale);
		}

		//座標をマスの中心に合わせる
		Vector3 pos = transform.position;
		pos.x = (int)(pos.x+2)/10 * 10;
		//pos.y = (int)(pos.y+2)/10 * 10;
		pos.z = (int)(pos.z+2)/10 * 10;
		transform.position = pos;
	}

	void OnGUI(){
		Rect rect = new Rect(10, 120, 100, 30);
		if(selectedFlg){
			GUI.Label(rect, "move", style);
		}
	}

	void move(){
		//groundを選択しているかどうか
		if (TouchManager.selectedGameObject.tag == "ground"){
			selectedFlg = false;
			target = TouchManager.selectedGameObject;
			targetPos = target.transform.position;
			//移動範囲がmoveRange以内かどうかの判定
			if(Mathf.Abs(targetPos.x - transform.position.x) + Mathf.Abs(targetPos.z - transform.position.z) <= (moveRange*10+1)){
				//実際に移動
				targetPos.y += 10;
				transform.position = targetPos;
			}
		}
	}
}