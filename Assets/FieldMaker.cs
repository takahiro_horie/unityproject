﻿using UnityEngine;
using System.Collections;

public class FieldMaker : MonoBehaviour {

	public GameObject prefab;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < 10; i++)
        {
        	for (int j = 0; j < 10; j++)
        	{
            	Instantiate(prefab, new Vector3(i*10,0,j*10), Quaternion.identity);
            }
        }
        Instantiate(prefab, new Vector3(2*10,10,1*10), Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
